import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './game/game.component';
import { HighScoresComponent } from './high-scores/high-scores.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';

const routes: Routes = [
	{
		path: 'game',
		component: GameComponent,
	},
	{
		path: 'register',
		component: RegisterUserComponent,
	},
	{
		path: 'login',
		component: LoginUserComponent,
	},
	{
		path: 'logout',
		component: LogoutComponent,
	},
	{
		path: 'profile',
		component: UpdateUserComponent,
	},
	{
		path: 'highScores',
		component: HighScoresComponent,
	},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
