import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { GameComponent } from './game/game.component';
import { HighScoresComponent } from './high-scores/high-scores.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';


@NgModule({
	declarations: [
		AppComponent,
		BoardComponent,
		RegisterUserComponent,
		GameComponent,
		LoginUserComponent,
		UpdateUserComponent,
		HighScoresComponent,
		LogoutComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule,
	],
	providers: [
		{
			provide: 'API_URL',
			useValue: 'http://localhost:9090',
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
