import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { comparePositions, Position } from './board';

@Component({
	selector: 'app-board',
	templateUrl: './board.component.html',
	styleUrls: ['./board.component.css']
})
export class BoardComponent {

	@Input() board?: Array<Array<string>>;

	@Output() onSwapped = new EventEmitter<[ Position, Position ]>();

	selected: Position | null = null;
	hoveredPiece: string | undefined = undefined;

	piecePressed(row: number, col: number): void {
		if(!this.selected) {
			this.selected = { row, col };
			return;
		}

		const pressed = { row, col };

		if(comparePositions(this.selected, pressed) === 0) {
			this.selected = null;
			return;
		}

		this.onSwapped?.emit([ this.selected, pressed ]);
		this.selected = null;
	}

	isSelected(row: number, col: number): boolean {
		return row === this.selected?.row && col === this.selected?.col;
	}

	pieceHovered(row: number, col: number): void {
		this.hoveredPiece = this.board?.at(row)?.at(col);
	}

	isHighlighted(row: number, col: number): boolean {
		return this.board ? this.board[row][col] === this.highlightedPiece() : false;
	}

	private highlightedPiece(): string | null {
		if(this.board && this.selected)
			return this.board[this.selected.row][this.selected.col];

		if(this.hoveredPiece)
			return this.hoveredPiece;

		return null;
	}
}
