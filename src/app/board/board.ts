
export type Position = {
	row: number,
	col: number,
};

export type PieceGenerator<T> = {
	next: () => T,
};

export type Match<T> = {
	piece: T,
	positions: Array<Position>,
};

export default class Board<T> {
	private _board: Array<Array<T>>;

	constructor(
		readonly width: number,
		readonly height: number,
		private _pieceGen: PieceGenerator<T>,
	) {
		this._board = [];

		for(let rowi = 0; rowi < height; rowi++) {
			const row = [];

			for(let coli = 0; coli < width; coli++)
				row.push(this._pieceGen.next());

			this._board.push(row);
		}
	}

	getBoard(): Array<Array<T>> {
		const cpy: Array<Array<T>> = [];
		this._board.forEach(row => cpy.push(row.slice()));
		return cpy;
	}

	getPiece(pos: Position): T | null {
		if(pos.row < 0 || pos.row >= this.height || pos.col < 0 || pos.col >= this.width)
			return null;

		return this._board[pos.row][pos.col];
	}

	canMove(p1: Position, p2: Position): boolean {
		if(p1.col !== p2.col && p1.row !== p2.row)
			return false;

		let matchFound = false;
		this.swapPieces(p1, p2);

		if(this.findMatch(p1) || this.findMatch(p2))
			matchFound = true;

		this.swapPieces(p1, p2);
		return matchFound;
	}

	move(p1: Position, p2: Position): number {
		if(p1.col !== p2.col && p1.row !== p2.row)
			throw new Error('Invalid move');

		if(!this.canMove(p1, p2))
			return 0;

		this.swapPieces(p1, p2);

		let matches = this.findMatches([ p1, p2 ]);
		if(matches.length === 0) {
			this.swapPieces(p1, p2);
			throw new Error('Invalid move');
		}

		let points = matches.length;

		do {
			let changed = this.processMatches(matches);

			matches = [];
			changed.forEach(pos => matches.push(...this.findMatchesAbove(pos)));

			points += matches.length;
		} while(matches.length > 0);

		return points;
	}

	private processMatches(matches: Array<Match<T>>): Map<number, Position> {
		const processed = new Set<Position>();

		matches.forEach(m =>
			m.positions.forEach(p => processed.add(p)));

		const changed = [ ...processed ]
			.sort(comparePositions);

		const changedCols = new Map<number, Position>();
		changed.forEach(p => {
			this.fill(p);

			if(p.row > (changedCols.get(p.col)?.row ?? 0))
				changedCols.set(p.col, p);
		});

		return changedCols;
	}

	private findMatchesAbove(pos: Position): Array<Match<T>> {
		const matches: Array<Match<T>> = [];

		const col = pos.col;
		for(let row = pos.row; row >= 0; row--) {
			const m = this.findMatch({ row, col });
			if(m)
				matches.push(m);
		}

		return matches;
	}

	private findMatches(positions: Array<Position>): Array<Match<T>> {
		return positions
			.map(pos => this.findMatch(pos))
			.filter(m => m !== null)
			.map(m => m!);
	}

	private fill(pos: Position) {
		for(let i = pos.row; i > 0; i--) {
			this._board[i][pos.col] = this._board[i - 1][pos.col];
		}

		this._board[0][pos.col] = this._pieceGen.next();
	}

	private swapPieces(p1: Position, p2: Position): void {
		const swp = this._board[p1.row][p1.col];
		this._board[p1.row][p1.col] = this._board[p2.row][p2.col];
		this._board[p2.row][p2.col] = swp;
	}

	private findMatch(pos: Position): Match<T> | null {
		const matchV = this.verticalMatch(pos);
		const matchH = this.horizontalMatch(pos);

		// if there is a match both vertically and horizontally merge them
		if(matchV && matchH) {
			return {
				piece: matchV.piece,
				positions: [ ...matchV.positions, ...matchH.positions ],
			};
		}

		return matchV ?? matchH;
	}

	private trans(pos: Position, drow: number, dcol: number = 0): Position {
		return {
			row: pos.row + drow,
			col: pos.col + dcol,
		};
	}

	private verticalMatch(pos: Position): Match<T> | null {
		const piece = this.getPiece(pos)!;

		if(piece === this.getPiece(this.trans(pos, -1))) { // check piece above
			if(piece === this.getPiece(this.trans(pos, 1))) {
				// piece above and below match
				return {
					piece,
					positions: [ this.trans(pos, -1), pos, this.trans(pos, 1) ],
				};
			} else if(piece === this.getPiece(this.trans(pos, -2))) {
				// 2 pieces above match
				return {
					piece,
					positions: [ this.trans(pos, -2), this.trans(pos, -1), pos ],
				};
			}
		} else if(piece === this.getPiece(this.trans(pos, 1))) { // check piece below
			if(piece === this.getPiece(this.trans(pos, 2))) {
				// 2 pieces below match
				return {
					piece,
					positions: [ pos, this.trans(pos, 1), this.trans(pos, 2) ],
				};
			}
		}

		return null;
	}

	private horizontalMatch(pos: Position): Match<T> | null {
		const piece = this.getPiece(pos)!;

		if(piece === this.getPiece(this.trans(pos, 0, -1))) { // check piece to the left
			if(piece === this.getPiece(this.trans(pos, 0, 1))) {
				// piece to the left and the right match
				return {
					piece,
					positions: [ this.trans(pos, 0, 1), pos, this.trans(pos, 0, -1) ],
				};
			} else if(piece === this.getPiece(this.trans(pos, 0, -2))) {
				// 2 pieces to the left match
				return {
					piece,
					positions: [ pos, this.trans(pos, 0, -1), this.trans(pos, 0, -2) ],
				};
			}
		} else if(piece === this.getPiece(this.trans(pos, 0, 1))) { // check piece to the right
			if(piece === this.getPiece(this.trans(pos, 0, 2))) {
				// 2 pieces to the right match
				return {
					piece,
					positions: [ this.trans(pos, 0, 2), this.trans(pos, 0, 1), pos ],
				};
			}
		}

		return null;
	}
}

export function comparePositions(p1: Position, p2: Position): number {
	if(p1.row - p2.row == 0)
		return p1.col - p2.col;
	return p1.row - p2.row;
}
