import Board from "./board.mjs";

export default function main(): void {
	const board = [
		[ 1, 2, 3, 4 ],
		[ 2, 3, 4, 5 ],
		[ 3, 4, 5, 6 ],
		[ 4, 3, 5, 3 ],
	];
	let bi = 0;

	const b = new Board<number>(4, 4, {
		next: () => {
			if(bi >= 4 * 4)
				return bi++ * -1;

			const p = board[Math.floor(bi / 4)][bi % 4];
			bi++;
			return p;
		}
	});

	printBoard(b);
	b.move({ row: 1, col: 3 }, { row: 1, col: 2 });
	printBoard(b);
	b.move({ row: 3, col: 1 }, { row: 3, col: 3 });
	printBoard(b);
}

function printBoard(b: Board<number>) {
	console.log('#################################');
	b.getBoard().forEach((row, idx) => {
		console.log(idx, row.map(p => p < 0 ? `${p}` : ` ${p}`));
	});
}

main();
