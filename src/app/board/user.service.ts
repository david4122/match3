import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { map, Observable, of, ReplaySubject, tap } from 'rxjs';

export type User = {
	id: number,
	username: string,
	password: string,
	admin: boolean,
};

@Injectable({
	providedIn: 'root'
})
export class UserService {

	private _token: string | null = null;

	private _currentUser: User | null = null;
	get currentUser() {
		return this._currentUser;
	}

	private _currentSubject = new ReplaySubject<User | null>(1);
	get current$() {
		return this._currentSubject.asObservable();
	}

	get isLoggedIn() {
		return this._currentUser !== null;
	}

	constructor(
		@Inject('API_URL') private apiUrl: string,
		private http: HttpClient
	) {
		this.current$.subscribe(user => {
			this._currentUser = user;
		});
	}

	register(username: string, password: string): Observable<User> {
		return this.http.post<User>(this.url('users'), {
			username, password
		});
	}

	login(username: string, password: string): Observable<{ token: string, userId: string }> {
		return this.http.post<{ token: string, userId: string }>(this.url('login'), {
			username, password
		}).pipe(
			tap(res => this._token = res.token),
			tap(res => this.getUser(res.userId).subscribe(
				user => this._currentSubject.next(user)))
		);
	}

	logout(): Observable<void> {
		if(!this._token)
			return of();

		return this.http.post<void>(
			this.url('logout'),
			null,
			this.optsWithToken({
				responseType: 'arraybuffer'
			})
		).pipe(
			tap({
				complete: () => this._currentSubject.next(null)
			})
		);
	}

	updatePassword(password: string): Observable<User> {
		if(!this._token)
			throw new Error('Not logged in');

		const updated: User = {
			...this.currentUser!,
			password,
		};

		return this.http.patch(
			this.url(`users/${this.currentUser?.id}`),
			updated,
			this.optsWithToken({ responseType: 'arraybuffer' }),
		).pipe(
			tap(() => this._currentSubject.next(updated)),
			map(() => updated)
		);
	}

	getUser(userId: string): Observable<User> {
		if(!this._token)
			throw new Error('Not logged in');

		return this.http.get<User>(
			this.url(`users/${userId}`),
			this.optsWithToken(),
		);
	}

	updateUser(user: User): Observable<User> {
		if(!this._token)
			throw new Error('Not logged in');

		const updated: User = {
			...this.currentUser!,
			...user,
		};

		return this.http.patch(
			this.url(`users/${this.currentUser?.id}`),
			updated,
			this.optsWithToken({ responseType: 'arraybuffer' }),
		).pipe(
			tap(() => this._currentSubject.next(updated)),
			map(() => updated)
		);
	}

	optsWithToken(opts = {}) {
		let params = new HttpParams();

		if(this._token)
			params = params.set('token', this._token);

		if((opts as any).params)
			params = { ...((opts as any).params), params }

		return { ...opts, params };
	}

	private url(slug: string = ''): string {
		return `${this.apiUrl}/${slug}`;
	}
}
