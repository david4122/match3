import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, ReplaySubject, Subscription, tap } from 'rxjs';
import Board, { Position } from './board/board';
import { UserService } from './board/user.service';

export type Game = {
	id: number,
	user: number,
	score: number,
	completed: boolean,
};

@Injectable({
	providedIn: 'root'
})
export class GameService {

	static readonly BOARD_HEIGHT = 5;
	static readonly BOARD_WIDTH = 5;

	private _board: Board<string> | null = null;
	private _game: Game | null = null;

	private _scoreSubject = new BehaviorSubject<number>(0);
	get score$() {
		return this._scoreSubject.asObservable();
	}

	private _boardSubject = new ReplaySubject<Array<Array<string>> | undefined>(1);
	get board$() {
		return this._boardSubject.asObservable();
	}

	constructor(
		@Inject('API_URL') private apiUrl: string,
		private http: HttpClient,
		private userService: UserService,
	) {
		this.score$.subscribe(s => {
			if(this._game)
				this._game.score = s;
		});
	}

	startNewGame(): Observable<Game> {
		return this.http.post<Game>(
			this.url('games'),
			null,
			this.userService.optsWithToken()
		).pipe(
			tap(g => {
				this._game = g;
				this._board = this.newBoard();
				this._boardSubject.next(this._board.getBoard());
			})
		);
	}

	finishGame(): Observable<Game> {
		if(!this._game)
			throw new Error('Game not started');

		this._game.completed = true;

		return this.updateGame(this._game).pipe(
			tap(g => {
				this._boardSubject.next(undefined);
				this._scoreSubject.next(0);
			})
		);
	}

	move(p1: Position, p2: Position): number {
		if(!this._board || !this._game)
			throw new Error('Game not started');

		const points = this._board.move(p1, p2);
		this._scoreSubject.next(this._scoreSubject.getValue() + points);
		this._boardSubject.next(this._board.getBoard());

		return points;
	}

	canMove(p1: Position, p2: Position) {
		if(!this._board)
			throw new Error('Game not started');

		return this._board.canMove(p1, p2);
	}

	getGames(): Observable<Array<Game>> {
		return this.http.get<Array<Game>>(
			this.url('games'),
			this.userService.optsWithToken()
		);
	}

	getGame(gameId: string): Observable<Game> {
		return this.http.get<Game>(
			this.url(`games/${gameId}`),
			this.userService.optsWithToken()
		);
	}

	updateGame(game: Game): Observable<Game> {
		return this.http.patch(
			this.url(`games/${game.id}`),
			game,
			this.userService.optsWithToken({
				responseType: 'arraybuffer'
			})
		).pipe(
			map(() => game)
		);
	}

	private newBoard(): Board<string> {
		return new Board(GameService.BOARD_WIDTH, GameService.BOARD_HEIGHT, {
			next: () => this.randomLetter()
		});
	}

	private randomLetter(): string {
		const start = 'A'.charCodeAt(0);
		const end = 'G'.charCodeAt(0) + 1;
		return String.fromCharCode(Math.random() * (end - start) + start);
	}

	private url(slug: string = ''): string {
		return `${this.apiUrl}/${slug}`;
	}
}
