import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Position } from '../board/board';
import { GameService } from '../game.service';

@Component({
	selector: 'app-game',
	templateUrl: './game.component.html',
	styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {

	board?: Array<Array<string>>;
	score = 0;

	private _subs: Array<Subscription> = [];

	constructor(
		private gameService: GameService,
		private router: Router,
	) { }

	ngOnInit(): void {
		this._subs.push(
			this.gameService.board$.subscribe(b => this.board = b),
			this.gameService.score$.subscribe(s => this.score = s)
		);
	}

	ngOnDestroy(): void {
		this._subs.forEach(sub => sub.unsubscribe());
	}

	swapPieces(poses: [ Position, Position ]): void {
		if(!this.gameService.canMove(...poses)) {
			alert('Invalid move');
			return;
		}

		this.gameService.move(...poses);
	}

	startGame(): void {
		this.gameService.startNewGame().subscribe({
			error: () => this.router.navigate(['/login'])
		});
	}

	endGame(): void {
		this.gameService.finishGame().subscribe(
			() => {
				this.board = undefined;
				this.router.navigate(['highScores']);
			});
	}
}
