import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../board/user.service';
import { Game, GameService } from '../game.service';

@Component({
	selector: 'app-high-scores',
	templateUrl: './high-scores.component.html',
	styleUrls: ['./high-scores.component.css']
})
export class HighScoresComponent implements OnInit, OnDestroy {

	topUser: Array<Game> = [];
	topAll: Array<Game> = [];

	private _userSub: Subscription | null = null;

	constructor(
		private gameService: GameService,
		private userService: UserService,
		private router: Router,
	) { }

	ngOnInit(): void {
		this._userSub = this.userService.current$.subscribe(
			u => {
				if(!u) {
					this.router.navigate(['login']);
					return;
				}

				this.gameService.getGames().subscribe(gms => {
					this.topUser = gms
						.filter(g => g.user === u.id)
						.sort(compareGames)
						.slice(0, 3);
				});
			},
			err => this.router.navigate(['login']));

		this.gameService.getGames().subscribe(
			gms => this.topAll = gms.sort(compareGames).slice(0, 10),
			err => this.router.navigate(['login']),
		);
	}

	ngOnDestroy(): void {
		this._userSub?.unsubscribe();
	}
}

function compareGames(g1: Game, g2: Game): number {
	return g2.score - g1.score;
}
