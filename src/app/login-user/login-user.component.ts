import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../board/user.service';

@Component({
	selector: 'app-login-user',
	templateUrl: './login-user.component.html',
	styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent {

	username: string = '';
	password: string = '';

	constructor(
		private userService: UserService,
		private router: Router,
	) { }

	loginUser() {
		this.userService.login(this.username, this.password).subscribe({
			next: () => this.router.navigate(['/game']),
			error: err => alert('Failed to login'),
		});
	}
}
