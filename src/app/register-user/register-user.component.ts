import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User, UserService } from '../board/user.service';

@Component({
	selector: 'app-register-user',
	templateUrl: './register-user.component.html',
	styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

	username: string = '';
	password: string = '';
	confirm: string = '';

	constructor(
		private userService: UserService,
		private router: Router,
	) { }

	ngOnInit(): void {
	}

	registerUser(): void {
		if(this.password !== this.confirm) {
			alert('Passwords do not match');
			return;
		}

		this.userService.register(this.username, this.password)
			.subscribe({
				next: () => this.router.navigate(['/login']),
				error: err => alert('Failed to register')
			});
	}
}
