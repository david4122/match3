import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User, UserService } from '../board/user.service';

@Component({
	selector: 'app-update-user',
	templateUrl: './update-user.component.html',
	styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit, OnDestroy {

	user?: User;

	private _userSub: Subscription | null = null;

	constructor(
		private userService: UserService,
		private router: Router,
	) {
		this._userSub = userService.current$.subscribe(u => {
			if(null === u)
				this.router.navigate(['login']);
			else
				this.user = u;
		});
	}

	ngOnInit(): void {
		if(!this.userService.isLoggedIn)
			this.router.navigate(['login']);
	}

	ngOnDestroy(): void {
		this._userSub?.unsubscribe();
	}

	updateUser(): void {
		if(!this.user) {
			this.router.navigate(['login']);
			return;
		}

		this.userService.updateUser(this.user)
			.subscribe(() => this.router.navigate(['game']));
	}
}
